# $Id$
# Maintainer: Pierre Schmitz <pierre@archlinux.de>
# Contributor: Sean V Kelley <seanvk@ortracks.org>

pkgname=openssl
_ver=1.0.1e
# use a pacman compatible version scheme
pkgver=${_ver/[a-z]/.${_ver//[0-9.]/}}
#pkgver=$_ver
pkgrel=6
pkgdesc='The Open Source toolkit for Secure Sockets Layer and Transport Layer Security'
arch=('i686' 'x86_64')
url='https://www.openssl.org'
license=('custom:BSD')
depends=('perl')
optdepends=('ca-certificates')
options=('!makeflags')
backup=('etc/ssl/openssl.cnf')
source=("https://www.openssl.org/source/${pkgname}-${_ver}.tar.gz"
        "https://www.openssl.org/source/${pkgname}-${_ver}.tar.gz.asc"
        'no-rpath.patch'
        'ca-dir.patch'
	'no-tls1.2.patch'
        'openssl-1.0.1e-fix_pod_syntax-1.patch'
        'openssl-1.0.1-Check-DTLS_BAD_VER-for-version-number.patch'
        'openssl-1.0.1-e_aes_cbc_hmac_sha1.c-fix-rare-bad-record-mac-on-AES.patch'
        'openssl-1.0.1-Check-EVP-errors-for-handshake-digests.patch'
        'openssl-1.0.1-Use-version-in-SSL_METHOD-not-SSL-structure.patch'
        'openssl-1.0.1-Fix-DTLS-retransmission-from-previous-session.patch')
md5sums=('66bf6f10f060d561929de96f9dfe5b8c'
         'SKIP'
         'dc78d3d06baffc16217519242ce92478'
         '3bf51be3a1bbd262be46dc619f92aa90'
         '88d3bef4bbdc640b0412315d8d347bdf'
         'ae7848bb152b8834ceff30c8c480d422'
         'c5cc62a47cef72f4e5ad119a88e97ae4'
         '3f674c14f07d9c7efd64c58e966eda83'
         '756362dfdd40cee380d3158022415fc4'
         'fc0c0466ea2f4f8446d16050a9639dee')

prepare() {
	cd $srcdir/$pkgname-$_ver

	# remove rpath: http://bugs.archlinux.org/task/14367
	patch -p0 -i $srcdir/no-rpath.patch
	# set ca dir to /etc/ssl by default
	patch -p0 -i $srcdir/ca-dir.patch
	# allow setting OPENSSL_NO_TLS1_2 in environment to disable TLS 1.2:
	# https://bugs.archlinux.org/task/33919
	patch -p2 -i "$srcdir/no-tls1.2.patch"
	patch -p1 -i $srcdir/openssl-1.0.1e-fix_pod_syntax-1.patch
	# OpenSSL 1.0.0k, 1.0.1.d, 1.0.1e fail handshake with DTLS1_BAD_VER
	# http://rt.openssl.org/Ticket/Display.html?id=2984
	patch -p1 -i $srcdir/openssl-1.0.1-Check-DTLS_BAD_VER-for-version-number.patch
	# Communication problems with 1.0.1e
	# http://rt.openssl.org/Ticket/Display.html?id=3002
	patch -p1 -i $srcdir/openssl-1.0.1-e_aes_cbc_hmac_sha1.c-fix-rare-bad-record-mac-on-AES.patch
	# CVE-2013-6449; FS#38357
	patch -p1 -i $srcdir/openssl-1.0.1-Check-EVP-errors-for-handshake-digests.patch
	patch -p1 -i $srcdir/openssl-1.0.1-Use-version-in-SSL_METHOD-not-SSL-structure.patch
	# CVE-2013-6450
	patch -p1 -i $srcdir/openssl-1.0.1-Fix-DTLS-retransmission-from-previous-session.patch
}

build() {
	cd $srcdir/$pkgname-$_ver

	if [ "${CARCH}" == 'x86_64' ]; then
		openssltarget='linux-x86_64'
		optflags='enable-ec_nistp_64_gcc_128'
	elif [ "${CARCH}" == 'i686' ]; then
		openssltarget='linux-elf'
		optflags=''
	fi

	# mark stack as non-executable: http://bugs.archlinux.org/task/12434
	./Configure --prefix=/usr --openssldir=/etc/ssl --libdir=lib \
		shared zlib ${optflags} \
		"${openssltarget}" \
		-Wa,--noexecstack "${CFLAGS}" "${LDFLAGS}"

	make depend
	make
}

check() {
	cd $srcdir/$pkgname-$_ver
	# the test fails due to missing write permissions in /etc/ssl
	# revert this patch for make test
	patch -p0 -R -i $srcdir/ca-dir.patch
	make test
	patch -p0 -i $srcdir/ca-dir.patch
}

package() {
	cd $srcdir/$pkgname-$_ver
	make INSTALL_PREFIX=$pkgdir MANDIR=/usr/share/man MANSUFFIX=ssl install
	install -D -m644 LICENSE $pkgdir/usr/share/licenses/$pkgname/LICENSE
}
md5sums=('66bf6f10f060d561929de96f9dfe5b8c'
         'SKIP'
         'dc78d3d06baffc16217519242ce92478'
         '3bf51be3a1bbd262be46dc619f92aa90'
         'c7ef6d29a59e7d78241393e79709c494'
         '88d3bef4bbdc640b0412315d8d347bdf'
         'ae7848bb152b8834ceff30c8c480d422'
         'c5cc62a47cef72f4e5ad119a88e97ae4'
         '3f674c14f07d9c7efd64c58e966eda83'
         '756362dfdd40cee380d3158022415fc4'
         'fc0c0466ea2f4f8446d16050a9639dee')
